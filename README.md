# update_fetcher

## Download

Go Check out [releases](https://gitlab.com/g4933/gen5w/update_fetcher/-/releases) and grab the latest compiled binaries (`.tar.gz` files)

*Mac users you will probably have to compile the tool yourself because of macOS security features... The app is not signed so it will probably not run without that. In any case, the binaries are provided for you. Make sure you pick the right architecture for better performance.* 

## Usage

The application is portable and can be run from terminal or command prompt/powershell

The general process is to open you execution environment of choice and navigate to the location of the downloaded release

##### Untar the file

Linux/OSX:

`tar xzvf UpdateFetcher_{YOUR_ARCHITECTURE}.tar.gz`

Windows:

Open with 7zip. (You may need to associate 7zip with .tar.gz files)

##### Downloading an Update release (Wizard)

Just run the app and follow the prompts

`./UpdateFetcher` or `./UpdateFetcher wizard`

##### Downloading an Update release (Manually)

You'll need to principally run 3 commands to start your download

1. `./UpdateFetcher list --region {YOUR_REGION}`

This will provide a list of all know vehicles for your region, find your model to use in the next command

2. `./UpdateFetcher inf --region {YOUR_REGION} --model {YOUR_MODEL}`

This will download all of the configuration (.inf) files so we can specify what we need to download. They will be stored in the executing directory under a folder called "downloads". This may take a couple minutes.

3. `./UpdateFetcher download --inf {PATH_TO_INF_FILE} --dest {UPDATE_DOWNLOAD_LOCATION}(optional)`

The last step will take a long time so just let it run and it will finish after downloading a worlds worth of map files

## Example

```bash
./UpdateFetcher list --region Eu
```
> Bayon_EU
>
> 2019_20_Elantra_EU
>
> ...
>
> Xceed_Ceed_CUV_MY22_EU


```bash
./UpdateFetcher inf --region Eu --model 2019_20_Elantra_EU
ls -lah ./downloads/
```

> downloads/2019_20_Elantra_EU/2110E_2019_20_Elantra_EU.inf
>
> downloads/2019_20_Elantra_EU/2110C_2019_20_Elantra_EU.inf
>
> ...
>
> downloads/2019_20_Elantra_EU/2205C_2019_20_Elantra_EU.inf
>
> downloads/2019_20_Elantra_EU/2205D_2019_20_Elantra_EU.inf

```bash
./UpdateFetcher download --inf ./downloads/2019_20_Elantra_EU/2205D_2019_20_Elantra_EU.inf
```


