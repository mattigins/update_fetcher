namespace UpdateFetcher.Regions;

public enum SupportedRegion
{
    Eu,
    Au,
    Ru,
    Kr,
    Us,
    Ca,
    Nz,
    Me,
    Br,
    In,
    Tr
}